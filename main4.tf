provider "azurerm" {
  features {}
}

variable "location" {
  description = "Location for all resources."
  default     = "francecentral"
}

resource "azurerm_resource_group" "rg" {
  name     = "OCC_ASD_Docker_Karim"
  location = var.location
}

resource "azurerm_virtual_network" "vnet" {
  name                = "KHVNet"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  address_space       = ["10.0.13.0/24"]
}

resource "azurerm_subnet" "vm_subnet" {
  name                 = "KHVMSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.13.64/28"]
}

resource "azurerm_subnet" "fw_subnet" {
  name                 = "AzureFirewallSubnet"  # The name of the Subnet for "ip_configuration.0.subnet_id" must be exactly 'AzureFirewallSubnet' to be used for the Azure Firewall resource
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.13.0/26"]
}

resource "azurerm_public_ip" "vm_public_ip" {
  name                = "VMPublicIP"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
}

resource "azurerm_public_ip" "fw_public_ip" {
  name                = "KHFirewallPublicIP"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku                 = "Standard"  # Spécifier explicitement le SKU de l'adresse IP publique comme "Standard"
}

resource "azurerm_network_interface" "vm_nic" {
  name                = "KHVMNIC"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = azurerm_subnet.vm_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.0.13.70"
    public_ip_address_id          = azurerm_public_ip.vm_public_ip.id
  }
}

resource "null_resource" "configure_firewall_nat_rule" {
  provisioner "local-exec" {
    command = "./dnat.sh"
  }

  # Exécuter le script à chaque fois que le fichier dnat.sh est modifié
  triggers = {
    script_file = "./dnat.sh"
  }
}
 