#!/bin/bash

# Définir les variables
resourceGroupName="OCC_ASD_Docker_Karim"
firewallName="KHFirewall"
natRuleName1="RDPNatRule"
sshRuleName="SSHRule"
httpRuleName="HTTPRule"
vmPrivateIpAddress="10.0.13.70"  # Adresse IP privée de la machine virtuelle Windows
vmNicId="/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_Docker_Karim/providers/Microsoft.Network/networkInterfaces/KHVMNIC"  # ID de votre interface réseau de VM
destinationPort1="3389"  # Port RDP par défaut
httpPort="80"  # Port HTTP
sshPort="22"  # Port SSH

# Créer la règle de redirection de port dans le pare-feu Azure pour RDP
az network firewall nat-rule create \
    --resource-group $resourceGroupName \
    --firewall-name $firewallName \
    --collection-name "AzureFirewallApplicationRuleCollection" \
    --name $natRuleName1 \
    --action "Dnat" \
    --priority 100 \
    --source-addresses "*" \
    --destination-addresses "52.143.167.129" \
    --destination-ports $destinationPort1 \
    --protocols "TCP" \
    --translated-address $vmPrivateIpAddress \
    --translated-port $destinationPort1

# Créer une règle pour autoriser le trafic HTTP
az network firewall application-rule create \
    --resource-group $resourceGroupName \
    --firewall-name $firewallName \
    --collection-name "AzureFirewallNetworkRuleCollection" \
    --name $httpRuleName \
    --priority 120 \
    --action "Allow" \
    --source-addresses "*" \
    --protocols "Http=80" \
    --target-fqdns "KHVMNIC"

# Associer la règle de redirection de port à l'interface réseau de la machine virtuelle
az network nic ip-config update \
    --resource-group $resourceGroupName \
    --nic-name "KHVMNIC" \
    --name "ipconfig1" \
    --private-ip-address $vmPrivateIpAddress
 