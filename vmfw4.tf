resource "azurerm_linux_virtual_machine" "vm" {
  name                = "KHDocker"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  size                = "Standard_DS1_v2"  # Utilisez une taille avec 6 Go de mémoire

  admin_username = "adminuser"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")  # Adjust the path to your public key file
  }

  network_interface_ids = [azurerm_network_interface.vm_nic.id]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}

resource "azurerm_firewall" "fw" {
  name                = "KHFirewall"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  sku_name = "AZFW_VNet"
  sku_tier = "Standard"

  ip_configuration {
    name                  = "FirewallConfig"
    subnet_id             = azurerm_subnet.fw_subnet.id
    public_ip_address_id  = azurerm_public_ip.fw_public_ip.id
  }
}
 