resource "null_resource" "ansible_provisioner" {
  provisioner "local-exec" {
    command = "ansible-playbook -i ${azurerm_public_ip.vm_public_ip.ip_address}, playbook.yml"
  }

  depends_on = [
    azurerm_linux_virtual_machine.vm,
  ]
}
 