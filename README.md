# Installation de Docker sur Linux

| Étape | Commande |
|-------|----------|
| **1. Mettre à jour l'index des paquets** | `sudo apt update` |
| **2. Installer les paquets requis pour utiliser un dépôt via HTTPS** | `sudo apt install apt-transport-https ca-certificates curl software-properties-common` |
| **3. Ajouter la clé GPG officielle de Docker** | `curl -fsSL https://download.docker.com/linux/ubuntu/gpg \| sudo apt-key add -` |
| **4. Ajouter le dépôt Docker aux sources APT** | `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"` |
| **5. Mettre à jour la base de données des paquets avec les paquets Docker du nouveau dépôt** | `sudo apt update` |
| **6. Installer Docker CE (Community Edition)** | `sudo apt install docker.io` |
| **7. Vérifier que Docker CE est installé correctement en exécutant l'image hello-world** | `sudo docker run hello-world` |
| **8. Activer le service Docker pour qu'il démarre au boot** (optionnel mais recommandé) | `sudo systemctl enable docker` |

Suivez ces étapes pour installer Docker sur votre système Linux. Docker devrait maintenant être installé et prêt à l'emploi.

| Command                                      | Description                                                              | Example                                        |
|----------------------------------------------|--------------------------------------------------------------------------|------------------------------------------------|
| `docker --version`                           | Show the Docker version installed.                                        | `docker --version`                             |
| `docker pull <image_name>`                   | Download an image from the Docker registry without creating a container.  | `docker pull node:16`                          |
| `docker images`                              | List all images on the local machine.                                     | `docker images`                                |
| `docker rmi <image_id>`                      | Remove an image from the local machine.                                   | `docker rmi <imageId or imageName>`            |
| `docker build -t <image_name>:<tag> <path>`  | Build an image from a Dockerfile.                                         | `docker build -t my-app:2 .`                   |
| `docker run <image_name>`                    | Run a container from an image.                                            | `docker run node:16`                           |
| `docker ps`                                  | List running containers.                                                  | `docker ps`                                    |
| `docker ps -a`                               | List all containers, including stopped ones.                              | `docker ps -a`                                 |
| `docker stop <container_id>`                 | Stop a running container.                                                 | `docker stop <containerId or name>`            |
| `docker start <container_id>`                | Start a stopped container.                                                | `docker start <containerId or containerName>`  |
| `docker restart <container_id>`              | Restart a container.                                                      | `docker restart <containerId or containerName>`|
| `docker kill <container_id>`                 | Forcefully stop a running container.                                      | `docker kill <containerId or containerName>`   |
| `docker rm <container_id>`                   | Remove a container from the machine.                                      | `docker rm <containerId or containerName>`     |
| `docker exec -it <container_id> bash`        | Run a command inside a running container. The `-it` flags make it interactive. | `docker exec -it containerName bash`           |
| `docker logs <container_id>`                 | Fetch logs from a container.                                              | `docker logs <containerName>`                  |
| `docker inspect <container_id>`              | Display detailed information on one or more containers or images.         | `docker inspect <containerName>`               |
| `docker cp <source_path> <container_id>:<destination_path>` | Copy files/folders between a container and the local filesystem.         | `docker cp example.txt CONTAINER_ID:/path/in/container/` |
| `docker system prune`                        | Remove all unused containers, networks, images, and optionally, volumes.  | `docker system prune`                          |
| `docker push <image_name>:<tag>`             | Push an image to the Docker registry.                                      | `docker push myuser/my-app:2`                  |
| `docker-compose up`                          | Create and start containers as defined in a `docker-compose.yml` file.     | `docker-compose up`                            |
| `docker-compose down`                        | Stop and remove containers defined in a `docker-compose.yml` file.         | `docker-compose down`                          |
| `docker network ls`                          | List all networks.                                                        | `docker network ls`                            |
| `docker network create <network_name>`       | Create a new network.                                                     | `docker network create <network_name>`         |
| `docker volume ls`                           | List all volumes.                                                         | `docker volume ls`                             |
| `docker volume create <volume_name>`         | Create a new volume.                                                      | `docker volume create <volume_name>`           |
| `docker stats`                               | Display a live stream of container(s) resource usage statistics.          | `docker stats`                                 |
| `docker info` | Fournit des informations détaillées sur l'installation Docker, y compris la version, le nombre de conteneurs, d'images, de volumes, etc. |
| `docker image ls` | Liste toutes les images disponibles dans le registre Docker local. |
| `docker container ls` | Liste tous les conteneurs en cours d'exécution. Vous pouvez ajouter l'option `-a` pour lister tous les conteneurs (en cours d'exécution et arrêtés). |
| `docker run <options> <image_name>` | Crée et démarre un nouveau conteneur à partir de l'image spécifiée. Les options peuvent inclure `-d` pour le mode détaché, `-p` pour le mappage des ports, `-v` pour le mappage des volumes, etc. |
| `docker image prune` | Supprime toutes les images orphelines (non utilisées) de votre registre Docker local pour libérer de l'espace. |
| `docker-compose ps` | Liste l'état des conteneurs définis dans un fichier `docker-compose.yml`. |
| `docker-compose logs <service_name>` | Affiche les logs d'un service spécifique défini dans votre fichier `docker-compose.yml`. |
| `docker-compose exec <service_name> <command>` | Exécute une commande dans un conteneur de service en cours d'exécution défini dans votre fichier `docker-compose.yml`. |
| `docker run -v <volume_name>:<container_path>` | Exécute un nouveau conteneur avec un volume spécifié monté sur un chemin spécifique dans le conteneur. |
| `docker volume rm <volume_name>` | Supprime un volume Docker spécifié. |
| `docker login` | Se connecte à un registre Docker, comme Docker Hub, avec vos identifiants. |
| `docker network connect <network_name> <container_name/id>` | Connecte un conteneur à un réseau spécifié. |
| `docker network disconnect <network_name> <container_name/id>` | Déconnecte un conteneur d'un réseau spécifié. |
| `docker stats <container_name/id>` | Affiche des statistiques en temps réel (CPU, mémoire, réseau, etc.) pour un ou plusieurs conteneurs. |
| `docker container prune` | Supprime tous les conteneurs arrêtés. |
| `docker volume prune` | Supprime tous les volumes non utilisés. |
 