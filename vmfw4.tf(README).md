Le fichier vmfw4.tf contient la définition de deux ressources Azure : une machine virtuelle Linux et un pare-feu Azure


| Ressource                              | Explication                                                                                                                                                      |
|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `azurerm_linux_virtual_machine` ("vm") | Cette ressource définit une machine virtuelle Linux sur Azure.                                                                                                  |
|                                       | - `name`: Le nom de la machine virtuelle est défini comme "KHDocker".                                                                                           |
|                                       | - `location`: Utilise l'emplacement (région) spécifié dans le groupe de ressources.                                                                             |
|                                       | - `resource_group_name`: Le groupe de ressources auquel appartient la machine virtuelle.                                                                        |
|                                       | - `size`: La taille de la machine virtuelle est définie sur "Standard_DS1_v2", qui dispose de 6 Go de mémoire.                                                  |
|                                       | - `admin_username`: Le nom d'utilisateur pour se connecter à la machine virtuelle est "adminuser".                                                              |
|                                       | - `disable_password_authentication`: Désactive l'authentification par mot de passe pour utiliser uniquement l'authentification par clé SSH.                      |
|                                       | - `admin_ssh_key`: Spécifie la clé SSH publique à utiliser pour se connecter à la machine virtuelle.                                                            |
|                                       | - `network_interface_ids`: Spécifie l'ID de l'interface réseau associée à la machine virtuelle.                                                                 |
|                                       | - `os_disk`: Configure le disque du système d'exploitation.                                                                                                      |
|                                       | - `source_image_reference`: Spécifie l'image du système d'exploitation à utiliser pour la machine virtuelle.                                                    |
| `azurerm_firewall` ("fw")             | Cette ressource définit un pare-feu Azure.                                                                                                                       |
|                                       | - `name`: Le nom du pare-feu est défini comme "KHFirewall".                                                                                                      |
|                                       | - `resource_group_name`: Le groupe de ressources auquel appartient le pare-feu.                                                                                  |
|                                       | - `location`: Utilise l'emplacement (région) spécifié dans le groupe de ressources.                                                                              |
|                                       | - `sku_name` et `sku_tier`: Spécifient le modèle de tarification et le niveau de service du pare-feu.                                                           |
|                                       | - `ip_configuration`: Configure la configuration IP du pare-feu, spécifiant le sous-réseau et l'adresse IP publique associés au pare-feu.                        |
 